import json
import os
import re
import statistics

raw_json_data = {}
analysed_data = {"per_wiki": {}, "all": {"unsetData": {}, "averageCounts": {}}}

# Load the raw JSON
for file in os.listdir("raw_data"):
    if file.endswith(".json"):
        data = json.JSONDecoder().decode(
            open(os.path.join("raw_data", file), "r").read()
        )
        raw_json_data.update({file.removesuffix(".json"): data})

for wiki, raw_json in raw_json_data.items():
    analysed_data["per_wiki"][wiki] = {"unsetData": {}, "averageCounts": {}}
    estimatedNumberOfReferenceIds = float(raw_json["totalRowCount"]["cu_useragent_clienthints_map"]) / \
                                    float(raw_json["averageMapRowsPerReferenceId"])
    # Work out how many reference IDs have a given uach_name unset.
    for name, counts in raw_json["mapTableRowCountBreakdown"].items():
        if isinstance(counts, dict):
            counts = [int(v) for k, v in counts.items()]
        else:
            counts = [int(v) for v in counts]
        combined_count = sum(counts)
        number_of_reference_ids_missing_this_name = round(estimatedNumberOfReferenceIds - (
                combined_count / float(raw_json["averageItemsPerNamePerReferenceId"][name])))
        analysed_data["per_wiki"][wiki]["unsetData"][name] = number_of_reference_ids_missing_this_name
        if name not in analysed_data["all"]["unsetData"]:
            analysed_data["all"]["unsetData"][name] = number_of_reference_ids_missing_this_name
        else:
            analysed_data["all"]["unsetData"][name] += number_of_reference_ids_missing_this_name
    # Attempt to remove grease data
    # Grease data is currently only in "brands" and "fullVersionList"
    for name in ["brands", "fullVersionList"]:
        for value in raw_json["mapTableRowCountBreakdown"][name].keys():
            if re.match('.*not.a.brand.*', value):
                del raw_json["mapTableRowCountBreakdown"][name][value]
    # Calculate the average number of reference IDs using a value, counting unset as one value
    for name, counts in raw_json["mapTableRowCountBreakdown"].items():
        if isinstance(counts, dict):
            counts = [int(v) for k, v in counts.items()]
        else:
            counts = [int(v) for v in counts]
        counts.append(analysed_data["per_wiki"][wiki]["unsetData"][name])
        average_count = statistics.mean(counts)
        analysed_data["per_wiki"][wiki]["averageCounts"][name] = average_count
        # Perform the average at the end
        if name not in analysed_data["all"]["averageCounts"]:
            analysed_data["all"]["averageCounts"][name] = counts
        else:
            analysed_data["all"]["averageCounts"][name] += counts

for name in analysed_data["all"]["averageCounts"].keys():
    analysed_data["all"]["averageCounts"][name] = statistics.mean(analysed_data["all"]["averageCounts"][name])

# Save the analysis to a file
json.dump(analysed_data, open("analysed_data.json", "w"))

# Output the results visually
for wiki in analysed_data["per_wiki"].keys():
    print("Order of entropy for", wiki)
    average_counts_sorted = [(v, k) for k, v in analysed_data["per_wiki"][wiki]["averageCounts"].items()]
    average_counts_sorted.sort()
    for average_count, name in average_counts_sorted:
        print(name)
    print("")

print("Order of entropy for all")
average_counts_sorted = [(v, k) for k, v in analysed_data["all"]["averageCounts"].items()]
average_counts_sorted.sort()
for average_count, name in average_counts_sorted:
    print(name)
